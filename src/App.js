import React from "react";
import { BrowserRouter } from "react-router-dom";
import AllRoutes from "./Component/AllRoutes";
import AuthState from "./Context/Auth/AuthState";

function App() {
  return (
    <>
      <BrowserRouter>
        <AuthState>
          <AllRoutes />
        </AuthState>
      </BrowserRouter>
    </>
  );
}

export default App;
