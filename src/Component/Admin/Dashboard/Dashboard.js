import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import DashboardInfo from "./DashboardInfo";
import DashboardReport from "./DashboardReport";
import DashboardMap from "./DashboardMap";

import BrowserUses from "./BrowserUses";
import DashboardDirectChat from "./DashboardDirectChat";
import DashboardLatestOrder from "./DashboardLatestOrder";

import InventoryInfo from "./InventoryInfo";
import ProductList from "./ProductList";

function Dashboard(props) {
  return (
    <>
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1 className="m-0">Dashboard v2</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item">
                  <Link to="/admin">Home</Link>
                </li>
                <li className="breadcrumb-item active">Dashboard</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <section className="content">
        <div className="container-fluid">
          <DashboardInfo />
          <DashboardReport />
          <div className="row">
            <div className="col-md-8">
              <DashboardMap />
              <DashboardDirectChat />
              <DashboardLatestOrder />
            </div>
            <div className="col-md-4">
              <InventoryInfo />
              <BrowserUses />
              <ProductList />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Dashboard;
