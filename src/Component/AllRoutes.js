import React, { useContext, useState } from "react";
import { Routes, Route } from "react-router-dom";
import AuthContext from "../Context/Auth/AuthContext";
import Dashboard from "./Admin/Dashboard/Dashboard";
import Profile from "./Admin/Profile/Profile";
import AddUser from "./Admin/User/AddUser";
import UserList from "./Admin/User/UserList";
import Login from "./Front/Login";
import FrontMaster from "./Layouts/FrontMaster";
import Master from "./Layouts/Master";

function AllRoutes() {
  return (
    <Routes>
      <Route path="/" element={<FrontMaster />}>
        <Route path="" element={<Login />}></Route>
      </Route>

      <Route path="/admin" element={<Master />}>
        <Route path="" element={<Dashboard />}></Route>
        <Route path="profile" element={<Profile />}></Route>
        <Route path="users" element={<UserList />}></Route>
        <Route path="users/add" element={<AddUser />}></Route>
      </Route>
    </Routes>
  );
}

export default AllRoutes;
