import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../../Context/Auth/AuthContext";

function Login(props) {
  const authData = useContext(AuthContext);
  useEffect(() => {
    document.body.classList.remove(
      "dark-mode",
      "sidebar-mini",
      "layout-fixed",
      "layout-navbar-fixed",
      "layout-footer-fixed"
    );
    document.body.classList.add("login-page");
  }, []);
  const postLogin = (event) => {
    event.preventDefault();
    authData.updateAuth();
  };
  return (
    <div className="login-box">
      <div className="card card-outline card-primary">
        <div className="card-header text-center">
          <span className="h1">
            <b>{process.env.REACT_APP_TITLE}</b>
          </span>
        </div>
        <div className="card-body">
          <p className="login-box-msg">Sign in to start your session</p>

          <form onSubmit={postLogin}>
            <div className="input-group mb-3">
              <input
                type="email"
                className="form-control"
                placeholder="Email"
              />
              <div className="input-group-append">
                <div className="input-group-text">
                  <span className="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div className="input-group mb-3">
              <input
                type="password"
                className="form-control"
                placeholder="Password"
              />
              <div className="input-group-append">
                <div className="input-group-text">
                  <span className="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-8">
                <div className="icheck-primary">
                  <input type="checkbox" id="remember" />
                  <label htmlFor="remember">Remember Me</label>
                </div>
              </div>

              <div className="col-4">
                <button className="btn btn-primary btn-block" type="submit">
                  Sign In
                </button>
                {/* <button type="submit" className="btn btn-primary btn-block">
                  Sign In
                </button> */}
              </div>
            </div>
          </form>

          <div className="social-auth-links text-center mt-2 mb-3"></div>

          <p className="mb-1">
            <a href="forgot-password.html">I forgot my password</a>
          </p>
          <p className="mb-0"></p>
        </div>
      </div>
    </div>
  );
}

export default Login;
