import React, { useContext } from "react";
import { Navigate, Outlet } from "react-router-dom";
import AuthContext from "../../Context/Auth/AuthContext";

function FrontMaster(props) {
  const authData = useContext(AuthContext);
  console.log();
  return <>{authData.isAuth ? <Navigate to="/admin" /> : <Outlet />}</>;
}

export default FrontMaster;
