import React, { useContext, useEffect } from "react";
import { Navigate, Outlet } from "react-router-dom";
import AuthContext from "../../Context/Auth/AuthContext";
import Footer from "./Footer";
import Nav from "./Nav";
import Sidebar from "./Sidebar";

function Master(props) {
  const authData = useContext(AuthContext);

  useEffect(() => {
    document.body.classList.add(
      "dark-mode",
      "sidebar-mini",
      "layout-fixed",
      "layout-navbar-fixed",
      "layout-footer-fixed"
    );
    document.body.classList.remove("login-page");
  }, []);
  return (
    <>
      {authData.isAuth ? (
        <div className="wrapper">
          <Nav />
          <Sidebar />
          <div className="content-wrapper">
            <Outlet />

            <Footer />
          </div>
          <aside className="control-sidebar control-sidebar-dark"></aside>
        </div>
      ) : (
        <Navigate to="/" />
      )}
    </>
  );
}

export default Master;
