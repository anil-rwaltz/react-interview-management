import React, { createContext } from "react";
const AuthContext = createContext({
  isAuth: false,
  username: "",
  image: "",
});

export default AuthContext;
