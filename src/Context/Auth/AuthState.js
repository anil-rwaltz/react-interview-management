import React, { useEffect, useState } from "react";
import AuthContext from "./AuthContext";

const AuthState = (props) => {
  const updateAuth = () => {
    SetAuthData({
      isAuth: true,
      username: "Anil",
      image: "",
      updateAuth: updateAuth,
    });
  };

  const [authData, SetAuthData] = useState({
    isAuth: false,
    username: "",
    image: "",
    updateAuth: updateAuth,
  });

  return (
    <AuthContext.Provider value={authData}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
